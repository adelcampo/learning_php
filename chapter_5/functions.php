<?php
  function page_header() {
    print "\n";
    print '<html><head><title>Welcome to my site</title></head>';
    print "\n";
    print '<body bgcolor="#ffffff">';
    print "\n";
  }
  
  function page_header2($color) {
  	print "\n";
    print '<html><head><title>Welcome to my site</title></head>';
    print "\n";
    print '<body bgcolor="#' . $color . '">';
    print "\n";
  }
  
  //Default Value
  function page_header3($color = 'cc3399') {
  	print "\n";
    print '<html><head><title>Welcome to my site</title></head>';
    print "\n";
    print '<body bgcolor="#' . $color . '">';
    print "\n";
  }

  function page_footer() {
  	print "\n";
    print '<hr>Thanks for visiting.';
    print "\n";
    print '</body></html>';
    print "\n";
  }

  function page_header4($color, $title) {
  	print "\n";
    print '<html><head><title>' . $title . '</title></head>';
    print "\n";
    print '<body bgcolor="#' . $color . '">';
    print "\n";
  }

  page_header();
  print "\n";
  print 'Welcome dude!';
  print "\n";
  page_footer();

  page_header2('cc00cc');
  page_header3();

  page_header4('66cc66','my homepage');

  function countdown($top) {
  	while($top > 0) {
  	  print "$top...";
  	  $top--;
  	}
  	print "Booom!\n";
  }
  
  $counter = 5;
  countdown($counter);
  print "\n";
  print "Outside , counter has first value: $counter";
  print "\n";

?>