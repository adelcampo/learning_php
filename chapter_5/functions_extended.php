<?php
//Multi optional arguments
  function page_header($color, $title, $header = 'Welcome') {
  	print '<html><head><title>Welcome to ' . $title . '</title></head>' . "\n";
  	print '<body bgcolor="#' . $color . '">' . "\n";
  	print "<h1>$header</head>\n";
  	print "*****************************\n";
  }
//How to call 
  page_header('aaccbb', 'All Boys');



function page_header7($color = '336699', $title = 'the page', $header = 'Welcome') {
print '<html><head><title>Welcome to ' . $title . '</title></head>' . "\n";
print '<body bgcolor="#' . $color . '">'  . "\n";
print "<h1>$header</h1>\n";
print "*****************************\n";
}
// Acceptable ways to call this function:
page_header7(); // uses all defaults
page_header7('66cc99'); // uses default $title and $header
page_header7('66cc99','my wonderful page'); // uses default $header
page_header7('66cc99','my wonderful page','This page is great!'); // no defaults



//Return values
function restaurant_check2($meal, $tax, $tip) {
  $tax_amount = $meal * ($tax / 100);
  $tip_amount = $meal * ($tip / 100);
  $total_notip = $meal + $tax_amount;
  $total_tip = $meal + $tip_amount;

  return array($total_notip, $total_tip);
}

function payment_method($cash_on_hand, $amount) {
  if ($cash_on_hand > $amount[1]) {
    return 'Cash!';
  } else {
  	return 'Credit Card!';
  }
}

$totals = restaurant_check2(15.22, 8.5, 15);
$method = payment_method(20, $totals);
print 'I will pay with ' . $method . "\n";
print "*****************************\n";


//Function return value compared on if statement
function complete_bill($meal, $tax, $tip, $cash_on_hand) {
  $tax_amount = $meal * ($tax / 100);
  $tip_amount = $meal * ($tip / 100);
  $total_amount = $meal + $tax_amount + $tip_amount;
  if ($total_amount > $cash_on_hand) {
    // The bill is more than we have
    return false;
  } else {
    // We can pay this amount
    return $total_amount;
  }
}

if ($total = complete_bill(15.22, 8.25, 15, 20)) {
  print "I'm happy to pay $total. \n";
} else {
  print "I don't have enough money. Shall I wash some dishes? \n";
}

print "*****************************\n";


//Variable Scope
$dinner = 'Curry Cuttlefish';

function vegetarian_dinner() {
  print "Dinner is $dinner, or ";
  $dinner = 'Sauteed Pea Shoots';
  print $dinner;
  print "\n";
}

function kosher_dinner() {
  print "Dinner is $dinner, or ";
  $dinner = 'Kung Pao Chicken';
  print $dinner;
  print "\n";
}

print "Vegetarian ";
vegetarian_dinner();
print "*****************************\n";
print "Kosher ";
kosher_dinner();
print "Regular dinner is $dinner \n";
print "*****************************\n";

//use of auto-global $GLOBALS array to access global scope variables 
function macrobiotic_dinner() {
  $dinner = "Some Vegetables.";
  print "Dinner is $dinner";
  print " but I'd rather have ";
  print $GLOBALS['dinner']; // or use global $dinner
  //global $dinner;
  print "\n";
}

macrobiotic_dinner();
print "Regular dinner is: $dinner \n";
print "*****************************\n";


//Declaring an argument and return "type" for function
function countdown(int $top): int {
  while ($top > 0) {
    print "$top..";
    $top--;
  }

  print "boom!\n";
}

$counter = 5;
countdown($counter);
print "Now, counter is $counter \n";
print "*****************************\n";
?>