<?php
  
  /*2. Write a PHP program that computes the total cost of this restaurant meal: two
  hamburgers at $4.95 each, one chocolate milkshake at $1.95, and one cola at 85
  cents. The sales tax rate is 7.5%, and you left a pre-tax tip of 16%.*/

  print ("\n2 - Resolution Output \n");
  $ham = 4.95 * 2;
  $choco = 1.95;
  $cola = 0.85;
  $price = $ham + $choco + $cola;
  $tax = 0.75;
  $total_price = $price * $tax;
  $tip = $price * 1.6;

	printf ("Total cost with taxes: %.2f", $total_price . "\n");
	print "Tip left: $tip\n";
?>