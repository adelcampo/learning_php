<?php
  /*
  3. Modify your solution to the previous exercise to print out a formatted bill. For
  each item in the meal, print the price, quantity, and total cost. Print the pre-tax
  food and drink total, the post-tax total, and the total with tax and tip. Make sure
  that prices in your output are vertically aligned.
  */
  print ("\n2 - Resolution Output: \n");
  $ham = 4.95 * 2;
  $choco = 1.95;
  $cola = 0.85;

  $price = $ham + $choco + $cola;
  $total_price = $price + (7.50 / 100);
  $tip = 16 / 100;

  print "Formatted Bill: \n";
  print "Items:   Cost:       Qty:\n";
  print "=====    ====        === \n";
  print "ham       4.95        2\n";
  print "choco     1.95        1\n";
  print "cola      0.85        1\n";
  print "Costs without taxes:  $price\n";
  print "Costs with taxes      $total_price\n";
  print "Suggested tip:        $tip\n";
?>








