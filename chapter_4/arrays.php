<?php

//Simple Definition
  //$vegetables = array('corn' => 'yellow',
	//'beet' => 'red',
	//'carrot' => 'orange');

  $dinner = array('0' => 'Sweet Corn and Sparagus',
	'1' => 'Lemon Chicken',
	'2' => 'Braised Bamboo Fungus');

  $computers = array('trs-80' => 'Radio Shack', 
	2600 => 'Atari', 
	'Adam' => 'Coleco');

//Square Brackets Definition
  $vegetables = ['corn' => 'yellow',
				 'beet' => 'red',
	             'carrot' => 'orange'];


//adding values to an array and iterate over it
$vegetables ['potato'] = 'white';
$keys = array_keys($vegetables);
print("\n" . '******************************************' . "\n");
print('FOR LOOP' . "\n");
print('Key' . ' Value' . "\n");
for ($i=0; $i < count($keys); $i++) { 
	print $keys[$i] . ' ' . $vegetables[$keys[$i]];
}


print("\n" . '******************************************' . "\n");
//iterate with foreach
print("FOREACH LOOP" . "\n");
foreach ($vegetables as $key => $value)
{
  echo($key . '  ' . $value . "\n");
  
}

//Self asigment keys as numeric
$dinner = array('Sweet Corn and Asparagus',
				'Lemon Chicken',
				'Braised Bamboo Fungus');
print "I want $dinner[0] and $dinner[2].";

// Create $lunch array with two elements
// This sets $lunch[0]
$lunch[] = 'Dried Mushrooms in Brown Sauce';
// This sets $lunch[1]
$lunch[] = 'Pineapple and Yu Fungus';


print("\n" . '******************************************' . "\n");
print('ARRAY SIZE' . "\n");
//count()
print(count($vegetables));

$test_array = [];
print ("\n" . count($test_array));
if (!$test_array) {
	print("\n" . 'empty array'); //evaluates to false
}

print("\n" . '******************************************' . "\n");
print("FOR EACH - TABLE" . "\n");

$row_styles = ['even', 'odd'];
$style_index = 0;

$meal = ['breakfast' => 'Walnut Bun', 
		 'lunch' => 'Cashew Nuts and White Muchrooms', 
		 'snack' => 'Dried Mulberries', 
		 'dinner' => 'Eggplant with Chili Sauce'];

print "<table>\n";
foreach ($meal as $key => $value) {
	print '<tr class="' . $row_styles[$style_index] . '">';
	print "<td>$key</td><td>$value</td></tr>\n";
	$style_index = 1 - $style_index;
}
print "</table>\n";


print("\n" . '******************************************' . "\n");
print("DISHES PRICES\n");

$meals = ['Walnut Bun' => 1,
          'Cashew Nuts and White Mushrooms' => 4.95,
          'Dried Mulberries' => 3.00,
		  'Eggplant with Chili Sauce' => 6.50];
foreach ($meals as $dish => $price) {
	$meals[$dish] = $meals[$dish] * 2;
}

foreach ($meals as $dish => $price) {
	printf("New price of %s is \$%.2f.\n", $dish, $price);
}


print("\n" . '******************************************' . "\n");
print("HOW TO ITERATE THROUGH ARRAYS WITH IN FOR LOOP\n");
$dinner = array('Sweet Corn and Asparagus',
'Lemon Chicken',
'Braised Bamboo Fungus');
for ($i = 0, $num_dishes = count($dinner); $i < $num_dishes; $i++) {
print "Dish number $i is $dinner[$i]\n";
}